const path = require('path');
const DirectoryNamedWebpackPlugin = require("directory-named-webpack-plugin");
const WrmPlugin = require("atlassian-webresource-webpack-plugin");
const UglifyJSPlugin = require("webpack/lib/optimize/UglifyJsPlugin");
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const baseCSS = new ExtractTextPlugin("base.css");
const vendorCSS = new ExtractTextPlugin("vendor.css");

const MVN_OUTPUT_DIR = path.join(__dirname, 'target', 'classes');
const FRONTEND_SRC_DIR = path.join(__dirname, 'src', 'main', 'resources');
const NODE_MODULES_DIR = path.join(__dirname, 'node_modules');

const providedDependencies = require(path.resolve(FRONTEND_SRC_DIR, "wrm-dependencies-conf.js"));

module.exports = {
    context: FRONTEND_SRC_DIR,
    entry: {
        'glossary-of-terms': path.join(FRONTEND_SRC_DIR, 'page', 'glossary-of-terms.js'),
        'glossary-of-terms2': path.join(FRONTEND_SRC_DIR, 'page', 'glossary-of-terms.js'), // fake a new entry point to get commons chunk to work
        'hilight-dialog': path.join(FRONTEND_SRC_DIR, 'widget', 'highlight-dialog', 'highlight-dialog.js'), // fake a new entry point to get commons chunk to work
    },
    externals: {
        'aui/flag': {
            amd: 'aui/flag',
            root: 'AJS.flag'
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            },
            {
                test: /\.(png|gif)$/,
                use: 'file-loader?name=images/[name].[ext]'
            },{
                test: /\.css$/,
                use: baseCSS.extract({
                    use:  ['css-loader', 'resolve-url-loader']
                }),
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: vendorCSS.extract({
                    use: ['css-loader', 'resolve-url-loader']
                }),
                include: /node_modules/
            },
        ]
    },
    plugins: [
        baseCSS,
        vendorCSS,
        new WrmPlugin({
            pluginKey: "com.atlassian.confluence.plugins.glossary-of-terms",
            providedDependencies: providedDependencies,
            xmlDescriptors: path.resolve(MVN_OUTPUT_DIR, "META-INF", "plugin-descriptors", "wr-webpack-bundles.xml")
        }),
        new webpack.optimize.CommonsChunkPlugin({name: 'vendor',  minChunks: 2}),
        new UglifyJSPlugin({
            sourceMap: true
        }),
    ],
    resolve: {
        alias: {
            'conf-glossary': FRONTEND_SRC_DIR,
            jquery$: path.join(FRONTEND_SRC_DIR, 'lib', 'jquery.js'),
            'vendor/es6-promise': path.join(NODE_MODULES_DIR, 'es6-promise', 'dist', 'es6-promise.auto.js'),
            'vendor/idb': path.join(NODE_MODULES_DIR, 'idb', 'lib', 'idb.js'),
        },
        modules: [
            path.resolve(FRONTEND_SRC_DIR),
            path.resolve(NODE_MODULES_DIR)
        ],
        plugins: [
            new DirectoryNamedWebpackPlugin()
        ]
    },
    output: {
        filename: 'bundle.[name].js',
        library: 'conf-glossary/glossary-of-terms',
        libraryTarget: 'amd',
        path: path.resolve(MVN_OUTPUT_DIR)
    }
};
