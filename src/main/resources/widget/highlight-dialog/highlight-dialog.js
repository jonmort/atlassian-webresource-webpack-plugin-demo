/* global define, Confluence */

define('conf-glossary/widget/highlight-dialog', ['require'], function(require) {
    require('./highlight-dialog.css');
    const $ = require('jquery');
    const Backbone = require('conf-glossary/lib/backbone');
    const ScrollingInlineDialog = Confluence.ScrollingInlineDialog;
    const DocThemeUtils = Confluence.DocThemeUtils;
    const Meta = AJS.Meta;
    const Templates = Confluence.GlossaryOfTermsPlugin.Templates.Widget.HighlightDialog;

    const DIALOG_MAX_HEIGHT = 200;
    const DIALOG_WIDTH = 300;
    let highlightDemoDialog;
    const defaultDialogOptions = {
        hideDelay: null,
        width : DIALOG_WIDTH,
        maxHeight: DIALOG_MAX_HEIGHT
    };

    // TODO: This should be in a form utils module
    function serializeFormElement(formElement) {
        const o = {};
        const a = $(formElement).serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }

    function getSpaceKey() {
        return Meta.get('space-key');
    }

    function closeFn() {
        highlightDemoDialog && highlightDemoDialog.remove();
    }

    function showHighlightDemoDialog(selectionObject) {
        closeFn();
        const displayFn = function(content, trigger, showPopup) {
            const html = Templates.createDialogContent({
                term: selectionObject.text,
                space: getSpaceKey(),
                foundNum: selectionObject.searchText.numMatches
            });
            const $content = $(content);
            $content.html(html);

            showPopup();

            // TODO: Better focus control.
            setTimeout(() => {
                $content.find("textarea").first().focus();
            }, 4);

            return false;
        };
        return highlightDemoDialog = _openDialog(selectionObject, 'define-term-dialog', defaultDialogOptions, displayFn);
    }

    function _openDialog(selectionObject, id, options, displayFn) {
        const $target = $("<div>");
        _appendDialogTarget(selectionObject.area.average, $target);
        const originalCallback = options.hideCallback;
        options.hideCallback = function() {
            $target.remove(); // clean up dialog target element when hiding the dialog
            originalCallback && originalCallback();
        };
        const dialog = ScrollingInlineDialog($target, id, displayFn, options);
        dialog.show();
        return dialog;
    }

    function _appendDialogTarget(targetDimensions, $target) {
        DocThemeUtils.appendAbsolutePositionedElement($target);
        $target.css({
            top: targetDimensions.top,
            height: targetDimensions.height,
            left: targetDimensions.left,
            width: targetDimensions.width,
            'z-index': -9999,
            position: 'absolute'
        });
    }

    return Backbone.View.extend({
        initialize: function() {
            this.handlers = [];
        },
        events: {
            "submit": "onSubmitWrapper"
        },
        onSubmitWrapper: function(e) {
            const form = e.target;
            const data = serializeFormElement(form);
            this.handlers.forEach(handler => {
                handler(data);
            });
            e.preventDefault();
            closeFn();
        },
        /**
         * Will register a handler function that will be
         * @param {Function} handlerFn - will be called when the form inside the dialog is submitted
         */
        onSubmit: function(handlerFn) {
            this.handlers.push(handlerFn);
        },
        render: function (obj) {
            this.setElement(showHighlightDemoDialog(obj));
            return this;
        },
        showHighlightDialog: function(obj) {
            return this.render(obj);
        }
    });
});
