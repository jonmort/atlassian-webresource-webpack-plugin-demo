define('conf-glossary/feature/library/local-library', ['jquery'], function($) {
    const TERMS = [];

    TERMS.push({
        name: "Confluence",
        slug: "confluence",
        category: "product",
        definitions: [{
            type: "text",
            content: "The best way to collaborate with your teammates and get on the same page ;)"
        }]
    });
    TERMS.push({
        name: "Autoconvert",
        slug: "autoconvert",
        category: "product",
        definitions: [{
            type: "text",
            content: "A feature of Confluence that automatically converts linked content in to rich content."
        }]
    });
    TERMS.push({
        name: "AMPS",
        slug: "amps",
        category: "acronym",
        definitions: [{
            category: "text",
            content: "Atlassian Maven Plugin Suite"
        }, {
            category: "text",
            content: "A plugin to Maven that helps turns a Maven project in to an Atlassian add-on."
        }]
    });
    TERMS.push({
        name: "Technical debt",
        slug: "technical-debt",
        category: "concept",
        definitions: [{
            type: "text",
            content: "The things that slow you down when you attempt to write or change code."
        }]
    });

    const loader = new $.Deferred().resolve(TERMS);
    return {
        load: function() {
            return loader.promise();
        },
        save: function(term) {
            TERMS.push(term);
        },
        saveAll: function() {
            // There's no saving to the reference library.
            return new $.Deferred().reject().promise();
        }
    };
});
