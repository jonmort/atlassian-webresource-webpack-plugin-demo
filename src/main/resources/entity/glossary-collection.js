define('conf-glossary/entity/glossary-collection', [
    'conf-glossary/entity/term',
    'conf-glossary/lib/backbone',
    'conf-glossary/lib/underscore',
    'jquery'
], function(Term, Backbone, _, $) {

    const GlossaryCollection = Backbone.Collection.extend({
        model: Term,

        persist: function() {
            const self = this;
            const promises = this.chain()
                .filter(model => model.isNew() || model.hasChanged())
                .each(model => model.save())
                .value();
            $.when.apply($, promises).then(r => {
                self.trigger('persisted', r);
            });
        },

        findByTerm: function findByTerm(termName) {
            if (!termName) return false;
            const search = termName.toLowerCase();
            return this.find(model => {
                return model.slug == search || model.get('name').toLowerCase() == search
            });
        }
    });

    return GlossaryCollection;
});
