const gulp = require('gulp');
const path = require('path');
const gulpWebpack = require('webpack-stream');
const webpack = require('webpack');

const webpackConfig = require('./webpack.config');

const MVN_OUTPUT_DIR = path.join('target', 'classes');
const FRONTEND_SRC_DIR = path.join('src', 'main', 'resources');

//
// Define low-level, compose-able sub-steps for our gulp tasks
//

function webpackOurCode(watching = false) {
    const cfg = Object.assign({}, webpackConfig, {
        watch: !!watching
    });
    const outDir = path.join(cfg.output && cfg.output.path || MVN_OUTPUT_DIR);
    return function doSomeWebpackery() {
        return gulp.src(FRONTEND_SRC_DIR)
            .pipe(gulpWebpack(cfg, webpack))
            .pipe(gulp.dest(outDir));
    }
}

/**
 * I use a parallel task here, because later I can also add compilation
 * processes for our non-JavaScript assets, like our CSS and templates.
 */
let processResources = gulp.parallel(webpackOurCode(false));
let watchAllResources = gulp.parallel(webpackOurCode(true));

//
// And finally, export so that gulp can run the tasks
//

gulp.task('build', processResources);
gulp.task('default', processResources);
gulp.task('watch', watchAllResources);
